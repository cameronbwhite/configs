export PATH="$PATH:/opt/Sublime2/:/opt/"
export MYCODE="~/MyCode"

alias apt-get='apt-fast'
alias install='sudo apt-fast install'
alias search='apt-cache search'
alias update='sudo apt-fast update'
alias upgrade='sudo apt-fast upgrade'
alias source='source ~/.bashrc'
alias btsync='btsync --config ~/.btsync/btsync.conf'

export PYTHONPATH="$PYTHONPATH:/home/cbw/Repos/PyBitmessage/src/:/home/cbw/MyCode/:/opt/Sublime2"
