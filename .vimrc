"  .vimrc 
"   This file is option to the text editor vim.
"
" {{{ General
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    set nocompatible            " Toggle compatibility with vi
    set wrap                " Cause long lines to wrap around. 
    set incsearch           " Cause search results to hilight as typed
                            " like (e.g. Google Instant Search)
    set history=50          " Remember 50 Lines of code 
    set hidden              " Allows to have dirty buffers open but 
                            " not displayed.
    set textwidth=72        " In INSERT mode the a newline will be 
                            " inserted before the line exceeds 72 
                            " characters long.
    set ruler               " Display the cursor currents position in 
                            " the toolbar.
    set showcmd             " Display the command being entered.
    set showmode            " TODO ??? 
    set foldmethod=marker   " Default fold method. Depending on 
                            " config the fold mode might change 
                            " depending on context.
    "set number             " Turn on line numbers TODO create a map
" }}} 
" {{{ Syntax
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    set autoindent          " Automatically insert indent syntacitally 
    syntax on               " Turn on syntax highlighting 

    " Enable enchaced command-line completion.
    " Note: Presumes vim is complied with +wildmenu.
    set wildmenu
" }}} 
" {{{ FileTypes 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  " File type dectection plugins
  " When enabling filetype vim will try to determine the file type by
  " the extension or by the magic line(e.g. #!/usr/bin/bash). 
  filetype on
  " Will enable loading the ftplugin for the specific file type. 
  " ~/.vim/ftplugin/[FILETYPE].vim if it exists 
  filetype plugin on
  " Will enable loading the indent file for specific file type.
  filetype indent on
" }}}
" {{{ Tab
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    " The number of spaces tab appears to be. 
    " Pseudo: if tabstop is 2 then '\t' == '  '
    " Pseudo: if tabstop is 3 then '\t' == '   '
    set tabstop=4           
    " Will cause the tab key to insert white spaces to represent 
    " the tab. 
    set expandtab
    " The number of spaces autoindent will place. 
    set shiftwidth=4
" }}}
" {{{ Spell
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  "set spell spelllang=en_us    " PROBLEM: If I enable all the time
                                " vim trys to spellcheck too much, 
                                " sometimes even the code. 
" }}}
" {{{ Color Theme
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  if &t_Co > 2 || has("gui_running") 
    set background=dark
    colorscheme wombat256mod
    " Search with colorfull Highlighting
    set hlsearch 
  endif
" }}} 
" {{{ GUI 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  if has("gui_running")  
    ""
    " Gui a
    " 'a' Autoselect:                   'a'
    " 'A' 
    " 'c' Console dialogs for messages
    " 'f' Forground:  
    " 'm' menu bar is present
    " 'i' Use a Vim icon " 'M'   " 't' 
    set go=IietFc
    ""
    " TODO find out where fonts are kept.
    set guifont=DejaVu\ Sans\ Mono\ Bold\ 18
  endif
" }}}
" {{{ NERDtree                                                           
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    nmap <Leader>tt :NERDTreeToggle    <cr>
    nmap <Leader>tT :NERDTreeToggle ~/ <cr>
" }}} 
" {{{ Keys Mapping 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = "," 
" FIX: remap n because mapleader = "," breaks "," functionality
nnoremap <C-e> j
vnoremap <C-e> ,
" {{{ Vim Management Mappings
noremap  <silent> <Leader>sv    source ~/.vimrc<cr>
noremap  <silent> <Leader>ev    edit ~/.vimrc<cr>
" }}}
" {{{ Window Management Mappings                                         
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SELECT: Below, Above, Left, Right
noremap   <silent> <Leader>j    :wincmd j<cr>
noremap   <silent> <Leader>k    :wincmd k<cr>
noremap   <silent> <Leader>h    :wincmd h<cr>
noremap   <silent> <Leader>l    :wincmd l<cr>

" CLOSE: Current 
noremap   <silent> <Leader>cc   :close<cr>

" CLOSE: Below, Above, Left, Right
noremap   <silent> <Leader>cj   :wincmd j<cr>:close<cr> 
noremap   <silent> <Leader>ck   :wincmd k<cr>:close<cr>
noremap   <silent> <Leader>ch   :wincmd h<cr>:close<cr>
noremap   <silent> <Leader>cl   :wincmd l<cr>:close<cr>

" MOVE:  Below, Above, Left, Right
noremap   <silent> <Leader>ml   <C-W>L
noremap   <silent> <Leader>mk   <C-W>k
noremap   <silent> <Leader>mh   <C-W>h
noremap   <silent> <Leader>mj   <C-W>j
" }}}
" {{{ Tab Management Mappings
noremap   <silent> <Leader>H    :tabprevious<cr>
noremap   <silent> <Leader>L    :tabnext<cr>
noremap   <silent> <Leader>tc   :tabnew<cr>
noremap   <silent> <Leader>td   :tabclose<cr>
" }}}
" }}}

" {{{ .STUFF 
"set tag+=~/.vim/systags "  
"set t_Co=256            "
":autocmd BufNewFile *.html 0r ~/vim/skeleton.html 
":autocmd BufNewFile *.html call ParseSkeleton()
":autocmd BufNewFile *.h 0r ~/vim/skeleton.h 
":autocmd BufNewFile *.h call ParseSkeleton()
":autocmd BufNewFile *.c 0r ~/vim/skeleton.c 
":autocmd BufNewFile *.c call ParseSkeleton()
":autocmd BufNewFile *.cpp 0r ~/vim/skeleton.cpp
":autocmd BufNewFile *.cpp call ParseSkeleton()
":autocmd BufNewFile *.py 0r ~/vim/skeleton.py
":autocmd BufNewFile *.py call ParseSkeleton()
":autocmd BufNewFile *.sh 0r ~/vim/skeleton.sh
":autocmd BufNewFile *.sh call ParseSkeleton()
":autocmd BufNewFile *.zsh 0r ~/vim/skeleton.zsh
":autocmd BufNewFile *.zsh call ParseSkeleton()
" }
